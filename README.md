# Benchmark of some kriging libraries

**The purpose of this benchmark is to compare the kriging performance of [gstlearn](https://gstlearn.org/)
against other geostatistics libraries.**

## Overview

The benchmark includes `gsltearn`, `gstools`, `pykrige` and `krigeo`
(small on-purpose BRGM library with a straightforward numpy implementation).

The benchmark allows for the comparison of :
- the time spent for solving the kriging system
- the time spent for estimating a large number of points

## Installation

The benchmark is "wrapped" in a python package to simplify its installation.

The recommended way for installation is to build the package from sources:
```sh
git clone https://wherever/the/git/repository/is/benchmark-kriging-libraries
cd benchmark-kriging-libraries
# Install without krigeo
python -m pip install .
# Optionally install krigeo
python -m pip install git+https://gitlab.com/brgm/geomodelling/spatial/krigeo.git
```

## Usage

Once the package is installed, there are 2 scripts you can run :
```sh
# Benchmark the kriging system resolution
python scripts/benchmark_system_resolution.py
# Benchmark the estimetation of a large number of points
python scripts/benchmark_system_resolution.py
```
The results are stored as `.csv` files in the `results` directory.
This directory also contains two "template" Excel files with predefined charts.
You can copy/paste the `.csv` results in them to get a graphic view.

## Contributing

See [CONTRIBUTING.md](CONTRIBUTING.md)

## License

This benchmark is provided under the GNU General Public License (GPLv3) that can be found in the [LICENSE](LICENSE) file.
By using, distributing, or contributing to this project, you agree to the terms and conditions of this license.
