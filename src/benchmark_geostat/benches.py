# -*- coding: utf-8 -*-
from time import perf_counter

import gstlearn as gstl
import gstools
import numpy as np
import pykrige

try:
    import krigeo
except ImportError:
    krigeo = False


def _print_stats(mean, std, nb_realizations):
    print(
        f" Time elapsed: {mean:.2e} s +/- {100 * std / mean:.2} % (over {nb_realizations} realisations)"
    )


def bench_gstlearn(samples, targets, params):
    model = gstl.Model.createFromParam(
        gstl.ECov.SPHERICAL, params["range"], params["sill"]
    )
    model.addDrift(gstl.DriftM())
    neigh = gstl.NeighUnique()  # gstools / pykrige only propose unique neighborhood
    dbin = gstl.Db.createFromSamples(
        nech=len(samples),
        order=gstl.ELoadBy.SAMPLE,
        tab=samples.reshape(-1),
        names=["x1", "x2", "z1"],
        locatorNames=["x1", "x2", "z1"],
        flag_add_rank=0,
    )
    dbout = gstl.Db.createFromSamples(
        nech=len(targets),
        order=gstl.ELoadBy.SAMPLE,
        tab=targets.reshape(-1),
        names=["x1", "x2"],
        locatorNames=["x1", "x2"],
        flag_add_rank=0,
    )
    nb_realizations = params["nb_realizations"]
    times = np.zeros(nb_realizations)
    for i in range(nb_realizations):
        t0 = perf_counter()
        gstl.kriging(dbin, dbout, model, neigh)
        t1 = perf_counter()
        times[i] = t1 - t0
        print(".", end="", flush=True)
    mean, std = np.mean(times), np.std(times)
    _print_stats(mean, std, nb_realizations)
    return mean, std, dbout.getColumnByLocator(gstl.ELoc.Z)


def bench_gstools(samples, targets, params):
    model = gstools.Spherical(dim=2, var=params["sill"], len_scale=params["range"])
    nb_realizations = params["nb_realizations"]
    times = np.zeros(nb_realizations)
    for i in range(nb_realizations):
        t0 = perf_counter()
        ok = gstools.krige.Ordinary(
            model, cond_pos=[samples[:, 0], samples[:, 1]], cond_val=samples[:, 2]
        )
        z = ok([targets[:, 0], targets[:, 1]])[0]
        t1 = perf_counter()
        times[i] = t1 - t0
        print(".", end="", flush=True)
    mean, std = np.mean(times), np.std(times)
    _print_stats(mean, std, nb_realizations)
    return mean, std, z


def bench_pykrige(samples, targets, params):
    model = {
        "variogram_model": "spherical",
        "variogram_parameters": {
            "sill": params["sill"],
            "range": params["range"],
            "nugget": 0.0,
        },
    }
    nb_realizations = params["nb_realizations"]
    times = np.zeros(nb_realizations)
    for i in range(nb_realizations):
        ok = pykrige.ok.OrdinaryKriging(
            x=samples[:, 0], y=samples[:, 1], z=samples[:, 2], **model
        )
        t0 = perf_counter()
        z, _ = ok.execute(style="points", xpoints=targets[:, 0], ypoints=targets[:, 1])
        t1 = perf_counter()
        times[i] = t1 - t0
        print(".", end="", flush=True)
    mean, std = np.mean(times), np.std(times)
    _print_stats(mean, std, nb_realizations)
    return mean, std, z


ALL_BENCHES = {
    "gstlearn": bench_gstlearn,
    "gstools": bench_gstools,
    "pykrige": bench_pykrige,
}

if krigeo:

    def bench_krigeo(samples, targets, params):
        covariance = krigeo.covariance_models["spherical"](
            sill=params["sill"], range=params["range"], nugget=0
        )
        nb_realizations = params["nb_realizations"]
        times = np.zeros(nb_realizations)
        for i in range(nb_realizations):
            ord_fct = krigeo.OrdinaryKriging(
                np.column_stack((samples[:, 0], samples[:, 1])),
                samples[:, 2],
                covariance,
                cache=False,
            )
            t0 = perf_counter()
            z = ord_fct(targets)
            t1 = perf_counter()
            times[i] = t1 - t0
            print(".", end="", flush=True)
        mean, std = np.mean(times), np.std(times)
        _print_stats(mean, std, nb_realizations)
        return mean, std, z

    ALL_BENCHES["krigeo"] = bench_krigeo
