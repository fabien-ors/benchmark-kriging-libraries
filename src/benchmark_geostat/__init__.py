# -*- coding: utf-8 -*-
try:
    from .__version__ import __version__, __version_tuple__, version, version_tuple
except ImportError:
    __version__ = version = None
    __version_tuple__ = version_tuple = ()

from pathlib import Path

__install_dir = Path(__file__).parent.parent.parent
RESULT_DIR = __install_dir / "results"
RESULT_DIR.mkdir(exist_ok=True)

from .benches import ALL_BENCHES
from .data import create_more_data, load_data_set
