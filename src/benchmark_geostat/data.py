# -*- coding: utf-8 -*-
from pathlib import Path

import gstlearn as gstl
import numpy as np
import pandas as pd


def load_data_set() -> np.ndarray:
    filename = Path(__file__).parent / "data/sic97data_01.dat"
    assert filename.exists()
    return pd.read_table(
        filename,
        sep=",",
        skiprows=6,
        index_col=0,
        names=["x", "y", "rainfall"],
        dtype="float64",
    ).to_numpy()


def create_more_data(samples, params, nb_total_data) -> np.ndarray:
    if nb_total_data < len(samples):
        return samples[:nb_total_data]

    model = gstl.Model.createFromParam(
        gstl.ECov.SPHERICAL, params["range"], params["sill"]
    )
    model.addDrift(gstl.DriftM())
    neigh = gstl.NeighUnique()  # gstools / pykrige only propose unique neighborhood
    dbin = gstl.Db.createFromSamples(
        nech=len(samples),
        order=gstl.ELoadBy.SAMPLE,
        tab=samples.reshape(-1),
        names=["x1", "x2", "z1"],
        locatorNames=["x1", "x2", "z1"],
        flag_add_rank=0,
    )

    rng = np.random.default_rng(42)
    xmin, ymin = np.min(samples, axis=0)[:2]
    xmax, ymax = np.max(samples, axis=0)[:2]
    xy = rng.random((nb_total_data - len(samples), 2))
    xy[:, 0] *= xmax - xmin
    xy[:, 0] += xmin
    xy[:, 1] *= ymax - ymin
    xy[:, 1] += ymin

    assert np.min(xy[:, 0]) >= xmin
    assert np.min(xy[:, 1]) >= ymin
    assert np.max(xy[:, 0]) <= xmax
    assert np.max(xy[:, 1]) <= ymax

    dbout = gstl.Db.createFromSamples(
        nech=len(xy),
        order=gstl.ELoadBy.SAMPLE,
        tab=xy.reshape(-1),
        names=["x1", "x2"],
        locatorNames=["x1", "x2"],
        flag_add_rank=0,
    )
    gstl.kriging(dbin, dbout, model, neigh)
    new_data = np.empty((nb_total_data, 3))
    new_data[: len(samples)] = samples
    new_data[len(samples) :, :2] = xy
    new_data[len(samples) :, 2] = dbout.getColumnByLocator(gstl.ELoc.Z)
    return new_data
